#!/usr/bin/env python
# -*- encoding: utf-8 -*-

""" Module dans lequel on stocke
    les variables qui doivent être conservées d'une requête HTTP à l'autre.
"""

#: On stocke dans ce dictionnaire une map
#: idbde -> socket de connexion au serveur NK2015.
CONNS = {}
