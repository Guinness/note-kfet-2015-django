/*******************************************************************************
                       Scritpt JS qui gère les consos
                        Utilisé en single et double
*******************************************************************************/

/*** Variables ***/
/* Par défaut, on considère qu'on n'est pas sur la page des dons */
var page_dons = false;
var ordered_stack = [];
/* Attention, changer l'emplacement de la ligne ci dessous provoque l'apocalypse */
window.onload=initStacks();


/*** Utilitaires ***/
/* Fonction qui teste si un tableau est vide (ou rempli de undefined) */
function isEmpty(tab) {
    for (var i in tab) {
        if (tab[i] != null) {
            return false;
        }
    }
    return true;
}
/* Fonction qui regarde si le code de retour est un succès */
function isSuccessCode(cod) {
    return ((cod == 0) || ((100 <= cod) && (cod < 200)))
}


/*** Gestion des Cookies ***/
function getCookie(sName) {
    var oRegex = new RegExp("(?:; )?" + sName + "=([^;]*);?");
    // A l'aide d'une regex, on match le nom du cookie
    if (oRegex.test(document.cookie)) {
        // Si le cookie existe, on stocke sa valeur
        var value = decodeURIComponent(RegExp["$1"])
        // Puis on écrase cette valeur
        setCookie(sName,JSON.stringify(null))
        // Il arrive qu'on ait stocké un cookie contenant la string null, on renvoit alors l'objet null
        if (JSON.parse(value) == null) {
            return null
        }
        return value;
    } else {
        return null;
    }

}

function setCookie(sName, sValue) {
    var today = new Date(), expires = new Date();
    expires.setTime(today.getTime() + (365*24*60*60*1000));
    document.cookie = sName + "=" + encodeURIComponent(sValue) + ";expires=" + expires.toGMTString();
}


/*** Gestion des stacks ***/
/* Function récupérant au chargement de la page les dernier stacks sauvegarder */
function initStacks() {
    // On commence par charger les cookies qui nous interesse
    var display_stack_aux = getCookie("display_stack")
    var display_stack_2_aux = getCookie("display_stack_2")
    var stack_aux = getCookie("cstack")
    var stack_2_aux = getCookie("cstack_2")
    var stack_button_aux = getCookie("stack_button")
    var ordered_stack_aux = getCookie("ordered_stack")
    if (display_stack_aux != null){
        // si les cookies n'étaient pas vide, on parse leur valeurs puis on récupère les données manquantes
        display_stack = JSON.parse(display_stack_aux);
        stack = JSON.parse(stack_aux);
        restore_display_stack(false);
    }
    if (display_stack_2_aux != null){
        display_stack_2 = JSON.parse(display_stack_2_aux);
        stack_2 = JSON.parse(stack_2_aux);
        restore_display_stack(true);
    }
    if (stack_button_aux != null){
        stack_button = JSON.parse(stack_button_aux);
        ordered_stack = JSON.parse(ordered_stack_aux);
        refreshStackButton();
    }
}
/* Fonction reconstruisant les spans correspondant à l'affichage des nom de notes, avec la couleur */
function restore_display_stack(secondstack) {
    var xhr = getXMLHttpRequest();
    // Selon l'argument, on reconstruit soit le premier soit le deuxième stack de note.
    if (secondstack) {
        var display_stack_used = display_stack_2;
    }
    else {
        var display_stack_used = display_stack;
    }
    xhr.onreadystatechange = function() {
        if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0)) {
            var answer = JSON.parse(xhr.responseText);
            if (answer == '"Erreur"') {
                display_error("Erreur au chargement des noms de note");
                return;
            }
            var liste = document.createElement("ul");
            liste.setAttribute("class", "liste_notes");
            var rep, elmt, texte, elemt_span, class1, class2, class3, tempfunction;

            for (var note in display_stack_used) {

                rep = answer[display_stack_used[note]["idbde"]];

                // on crée un nouvel élément
                elemt = document.createElement("li");
                texte = document.createTextNode(display_stack_used[note]["affiche"]);
                // on fait de la mise en forme à coup de spans
                elemt_span = document.createElement("span");
                // on le met aussi en forme en fonction de si il est negatif ou pas
                class2 = "liste_negatif" + rep["negatif"];
                if (rep["vieux"]) {
                    class3 = "liste_potvieux";
                } else { 
                    class3 = "";
                }
                // on donne les deux classes à l'élément span
                elemt_span.setAttribute("class", "liste " + class1 + " " + class2 + " " + class3);
                tempfunction = function () {
                    // permet de faire une clôture
                    var replocal = rep;
                    var elemt_local = elemt_span;
                    // Ici, le span pour un nom de note est construit, on le sauvegarde dans le display_stack concerné
                    display_stack_used[note]["source"]=elemt_span;
                };
                tempfunction();
                elemt_span.appendChild(texte);
            }
            // une fois tout les span crées, on rafraichit le stack
            refreshStack(secondstack);
        }
    };
    // On construit la liste des idbe a partir du display_stack
    var data = []
    for (var note in display_stack_used) {
    data.push(display_stack_used[note]["idbde"])
    }
    xhr.open("POST", NOTE_ROOT_URL + "get_display_info/", true);
    xhr.setRequestHeader("X-CSRFToken", csrftoken);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xhr.send("asked=" + encodeURIComponent(JSON.stringify(data)));

}

function saveStack(sName,sValue) {
setCookie(sName, JSON.stringify(sValue));
}


/*** Envoi des requête XMLHttpRequest ***/
/* Fonction appelée à la fin du timer (déclenché sur un onKeyUp sur le champ de recherche) */
function getInfo(secondstack, type) {
    // par défaut secondstack est undefined, donc on utilisera le premier
    var ident = "id_search_field"
    if (secondstack) {
        ident += "_2";
    }
    var search_field = document.getElementById(ident);
    var asked = search_field.value;
    /* on ne fait la requête que si on a au moins un caractère pour chercher */
    if (asked.length >= 1) {
        function readDataWrapper(oData) {
            var targetid = "liste_notes"
            if (secondstack) {
                targetid += "_2";
            }
            /* Si on est sur la page de consos, on veut afficher les comptes sur un mouseover,
               mais pas sur la page de dons */
            var display = !page_dons;
            return readData(oData, targetid, display, secondstack);
        }
        request(type, asked, readDataWrapper);
    }
}

/* Fonction appelée quand on clique sur une note */
function put_note(itself, affiche, idbde, nom, prenom, secondstack,solde,time_negatif,aka) {
    // par défaut secondstack est undefined, donc on utilisera le premier
    if (secondstack) {
        var stack_used = stack_2;
        var display_stack_used = display_stack_2;
    }
    else {
        var stack_used = stack;
        var display_stack_used = display_stack;
    }
   if (double_stack_mode && secondstack && !isEmpty(stack_button)) {
            // Il y avait des boutons dans le stack_button, mais on va utiliser le stack pour des notes. On le vide.
        emptyStack(false, true);
    }
    if (!double_stack_mode)
    {
       // On encastre les tests parce que stack_button n'existe pas forcément
        if  (!isEmpty(stack_button)) {
            // Il y a des consos dans le stack et on vient de cliquer sur une note
            // il faut donc lancer les transactions
            do_conso_many_boutons(idbde, affiche);
            return
        }
    }
    var before = stack_used[idbde];
    if (before == null)
    {
        stack_used[idbde] = 1;
    }
    else
    {
        stack_used[idbde] += 1;
    }
    var display_before = display_stack_used[affiche]
    if (display_before == null) {
        var tab = {};
        tab["nb"] = 1;
        tab["idbde"] = idbde;
        tab["source"] = itself;
        tab["affiche"] = affiche;
        tab["solde"] = solde;
        tab["time_negatif"] = time_negatif;
        tab["aka"] = aka;
        display_stack_used[affiche] = tab;
    }
    else
    {
        display_stack_used[affiche]["nb"] += 1;
    }
    refreshStack(secondstack);
    if (!page_dons) {
        /* On remplit aussi les champs des formulaires de crédit et de retrait */
        var target, pseudo_field, nom_field, prenom_field, idbde_field;
        for (var i_target in [0, 1]) {
            target = ["credit", "retrait"][i_target];
            pseudo_field = document.getElementById(target + "_pseudo");
            pseudo_field.replaceChild(itself.cloneNode(true), pseudo_field.firstChild);
            nom_field = document.getElementById("id_" + target + "_form-nom");
            nom_field.value = nom;
            prenom_field = document.getElementById("id_" + target + "_form-prenom");
            prenom_field.value = prenom;
            idbde_field = document.getElementById("id_" + target + "_form-idbde");
            idbde_field.value = idbde;
        }
    }
        /* Si on est en mode single_stack,
           on recopie le nom de note dans l'onglet de transfert
           et on peuple les variables transfert_from et transfert_to */
        if (!double_stack_mode) {
            // On remplit d'abord le TO si il est vide
               if ((transfert_to == null) && (transfert_from != idbde)) {
                    var note_destinataire = document.getElementById("transfert_destinataire");
                    note_destinataire.replaceChild(itself.cloneNode(true), note_destinataire.firstChild);
                    transfert_to = idbde;
                }
            
            else if( !page_dons ) {
                // Si il contient déjà quelque chose, alors on remplit le FROM

                var note_emetteur = document.getElementById("transfert_emetteur");
                note_emetteur.replaceChild(itself.cloneNode(true), note_emetteur.firstChild);
                transfert_from = idbde;
            }
        }
}

/* Fonction appelée quand on clique sur une note déjà dans le stack */
function remove_note(affiche, idbde, secondstack) {
    // par défaut secondstack est undefined, donc on utilisera le premier
    if (secondstack) {
        var stack_used = stack_2;
        var display_stack_used = display_stack_2;
    }
    else {
        var stack_used = stack;
        var display_stack_used = display_stack;
    }
    stack_used[idbde] -= 1;
    display_stack_used[affiche]["nb"] -= 1;
    if (stack_used[idbde] == 0) // il faut alors s'en débarasser
    {
        delete stack_used[idbde];
        // Si la note qu'on vient de faire disparaître correspond à transfert_from ou transfert_to,
        // il faut le remettre à null
        if (!double_stack_mode) {
            if (transfert_to == idbde) {
                erase_transfert_note(true);
            }
            if (transfert_from == idbde) {
                erase_transfert_note(false);
            }
        }
    }
    if (display_stack_used[affiche]["nb"] == 0) // il faut alors l'effacer
    {
        delete display_stack_used[affiche];
    }
    refreshStack(secondstack);
}

/* Fonction qui raffiche le stack avec des notes */
function refreshStack(secondstack) {
    // par défaut secondstack est undefined, donc on utilisera le premier
    if (secondstack) {
        var ident = "stack_2";
        var display_stack_used = display_stack_2;
    }
    else {
        var ident = "stack";
        var display_stack_used = display_stack;
    }
    var old_stack = document.getElementById(ident);
    var new_stack = document.createElement("ul");
    new_stack.setAttribute("id", ident);
    new_stack.setAttribute("class", "list-unstyled");
    var elmt, source_elmt;
    for (var note in display_stack_used) {
        elmt = document.createElement("li");
        source_elmt = display_stack_used[note]["source"].cloneNode(true);
        source_elmt.replaceChild(document.createTextNode(source_elmt.firstChild.nodeValue + " (x" + display_stack_used[note]["nb"] + ")"),
                                 source_elmt.firstChild);
        source_elmt.addEventListener("click", remove_note.bind("trapped", display_stack_used[note]["affiche"], display_stack_used[note]["idbde"], secondstack));


        tempfunction = function () {
            // permet de faire une clôture
            var dnote = display_stack_used[note];

        source_elmt.addEventListener("mouseover", function() {
            // On appelle displayAccount dans la clôture
            displayAccount(dnote["source"], dnote["affiche"], dnote["idbde"],dnote["solde"], dnote["time_negatif"], dnote["aka"]);
                 });
        };
        tempfunction();
        elmt.appendChild(source_elmt);
        new_stack.appendChild(elmt);
    }
    old_stack.parentNode.replaceChild(new_stack, old_stack);
}

/* Fonction pour vider les stacks */
function emptyStack(secondstack, buttons) {
    if (secondstack) {
        stack_2 = {};
        display_stack_2 = {};
    }
    else {
        stack = {};
        display_stack = {};
    }
    refreshStack(secondstack);
    if (buttons) {
        stack_button = {};
        ordered_stack = [];
        refreshStackButton();
    }
}

/* Fonction appelée quand on clique sur un bouton */
function put_conso(label, idbut) {
    if (double_stack_mode) {
        if (!isEmpty(stack_2)) {
            // Il y avait des notes dans le stack_2. On le vide.
            emptyStack(true);
        }
    }
    else {
        if (!isEmpty(stack)) {
            // Il y a des notes dans le stack et on vient de cliquer sur une conso
            // il faut donc lancer les transactions
            do_conso_many_notes(idbut, label);
            return
        }
    }
    var before = stack_button[idbut]
    if (before == null) {
        var tab = {};
        tab["nb"] = 1;
        tab["label"] = label;
        stack_button[idbut] = tab;
        ordered_stack.push(idbut);
    }
    else
    {
        stack_button[idbut]["nb"] += 1;
    }
    refreshStackButton()
}

/* Fonction appelée quand on clique sur une conso déjà dans le stack */
function remove_conso(label, idbut) {
    stack_button[idbut]["nb"] -= 1;
    if (stack_button[idbut]["nb"] == 0) // il faut alors s'en débarasser
    {
        delete stack_button[idbut];
        for (var i=0; i < ordered_stack.length; i++) {
            if (ordered_stack[i] == idbut) {
                ordered_stack.splice(i, 1);
            }
        }
    }
    refreshStackButton();
}

/* Fonction qui raffiche le stack avec des consos */
function refreshStackButton() {
    if (double_stack_mode) {
        // en mode double stack, les boutons sont toujours dans le stack 2
        var ident = "stack_2";
    }
    else {
        var ident = "stack";
    }
    var old_stack = document.getElementById(ident);
    var new_stack = document.createElement("ul");
    new_stack.setAttribute("id", ident);
    new_stack.setAttribute("class", "list-unstyled");
    var elmt, source_elmt;
    for (var i=0; i < ordered_stack.length; i++) {
        conso = stack_button[ordered_stack[i]];
        elmt = document.createElement("li");
        span_elmt = document.createElement("span");
        span_elmt.appendChild(document.createTextNode(conso["label"] + " (x" + conso["nb"] + ")"));
        span_elmt.addEventListener("click", remove_conso.bind("trapped", conso["label"], ordered_stack[i]));
        elmt.appendChild(span_elmt);
        new_stack.appendChild(elmt);
    }
    old_stack.parentNode.replaceChild(new_stack, old_stack);
}

/* Fonction de nettoyage des fomulaires */
function cleanForms() {
    /* On vide les forms */
    var ids = ["idbde", "montant", "commentaire", "type", "nom", "prenom", "banque"];
    var id, field;
    for (var target in [0, 1]) {
        for (var i_id = 0, c = ids.length; i_id < ids.length; i_id++) {
            id = "id_" + ["credit", "retrait"][target] + "_form-" + ids[i_id];
            field = document.getElementById(id);
            field.value = null;
        }
    }
    /* On vide aussi les zones de texte */
    var span_ids = ["credit_pseudo", "retrait_pseudo"];
    var emptyspan = document.createElement("span");
    var span;
    for (var i_id = 0, c = span_ids.length; i_id < span_ids.length; i_id++) {
        id = span_ids[i_id];
        span = document.getElementById(id);
        span.replaceChild(emptyspan.cloneNode(true), span.firstChild);
    }
    /* On vide également le formulaire de transfert */
    ids = ["montant", "commentaire"];
    for (var i_id = 0, c = ids.length; i_id < ids.length; i_id++) {
        id = "id_transfert_form-" + ids[i_id];
        field = document.getElementById(id);
        field.value = null;
    }
    /* On efface le contenu de "Transfert de * à *" si on est en single stack mode */
    if (!double_stack_mode) {
        erase_transfert_note(true);
        erase_transfert_note(false);
    }
}


/*** Faisage des consos ***/
/* Fonction qui fait une conso avec plusieurs boutons et une seule note */
function do_conso_many_boutons(idbde, matching_term) {
    // On se prépare à poster une requête
    var xhr = getXMLHttpRequest();
    
    // On définit ce qu'on fera quand on recevra la réponse = gestion de l'éventuelle erreur
    xhr.onreadystatechange = function() {
        if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0)) {
            // Si on reçoit une erreur, on sauvegarde les stacks et on redirige
            if (xhr.responseText=='"Erreur"') {
                saveStack("stack_button",stack_button)
                saveStack("ordered_stack",ordered_stack)
                window.location.replace(gotoURI("consos"));
                return;
            }
            var answer = JSON.parse(xhr.responseText);
            if (!isSuccessCode(answer["retcode"])) {
                display_error(answer["errmsg"]);
                return;
            }
            answer = answer["msg"];
            // On enlève du stack_button les consos qui ont marché et si il y a eu des échecs on alerte
            var errorhappened = false;
            var errlist = [];
            for (var i_ans in answer) {
                ans = answer[i_ans];
                if (isSuccessCode(ans[0])) {
                    delete stack_button[ans[1][0]];
                    for (var i=0; i < ordered_stack.length; i++) {
                        if (ordered_stack[i] == ans[1][0]) {
                            ordered_stack.splice(i, 1);
                        }
                    }
                }
                else {
                    errorhappened = true;
                    var bouton = stack_button[ans[1][0]];
                    errlist.push(matching_term + ", " + bouton["nb"] + "x" + bouton["label"] + " : " + ans[2]);
                }
            }
            if (errorhappened) {
                display_error("Attention :", errlist);
            }
            // et on rafraîchit le stack_button
            refreshStackButton();
            // et l'historique
            refreshHistorique();
        }
    };
    // On fabrique la requête en utilisant l'idbde et en parsant stack_button
    xhr.open("POST", NOTE_ROOT_URL + "do_conso/", true);
     xhr.setRequestHeader("X-CSRFToken", csrftoken);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    var consodata = ""
    for (var i_but in stack_button) {
        if (consodata != "") {
            consodata += ",";
        }
        consodata += "(" + i_but + "," + idbde + "," + stack_button[i_but]["nb"] + ")";
    }
    var post_data = "consodata=" + encodeURIComponent(consodata);
    xhr.send(post_data);
}

/* Fonction qui fait une conso avec plusieurs notes et un seul bouton */
function do_conso_many_notes(idbut, labelbut) {
    // On se prépare à poster une requête
    var xhr = getXMLHttpRequest();
    
    // On définit ce qu'on fera quand on recevra la réponse = gestion de l'éventuelle erreur
    xhr.onreadystatechange = function() {
        if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0)) {
            if (xhr.responseText=='"Erreur"') {
            // Si on reçoit une erreur, on sauvegarde les stacks et on redirige
            // Ici, il faut également simplifier les stacks pour qu'il rentre dans les cookies
                for (var note in display_stack) {
                    delete display_stack[note]["source"]
                }
                saveStack("display_stack",display_stack)
                saveStack("cstack",stack)
                window.location.replace(gotoURI("consos"));
                return;
            }
            var answer = JSON.parse(xhr.responseText);
            if (!isSuccessCode(answer["retcode"])) {
                display_error(answer["errmsg"]);
                return;
            }
            answer = answer["msg"];
            // On enlève du stack les consos qui ont marché et si il y a eu des échecs on alerte
            var errorhappened = false;
            var errlist = [];
            for (var i_ans in answer) {
                ans = answer[i_ans];
                if (isSuccessCode(ans[0])) {
                    delete stack[ans[1][1]];
                    // Il faut aussi enlever tous les affichages qui ont cet idbde
                    for (var i_note in display_stack) {
                        note = display_stack[i_note];
                        if (note["idbde"] == ans[1][1]) {
                            delete display_stack[i_note];
                        }
                    }
                }
                else {
                    errorhappened = true;
                    // Il faut trouver les noms de note correspondants
                    var nom = "";
                    var nb = 0;
                    for (var i_note in display_stack) {
                        note = display_stack[i_note];
                        if (note["idbde"] == ans[1][1]) {
                            if (nom != "") {
                                nom += " aka ";
                            }
                            nom += note["affiche"];
                            nb += 1;
                        }
                    }
                    errlist.push(nom + ", " + nb + "x" + labelbut + " : " + ans[2]);
                }
            }
            if (errorhappened) {
                display_error("Attention :", errlist);
            }
            // et on rafraîchit le stack
            refreshStack();
            // et l'historique
            refreshHistorique();
        }
    };
    // On fabrique la requête en utilisant l'idbut et en parsant stack
    xhr.open("POST", NOTE_ROOT_URL + "do_conso/", true);
     xhr.setRequestHeader("X-CSRFToken", csrftoken);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    var consodata = ""
    for (var i_compte in stack) {
        if (consodata != "") {
            consodata += ",";
        }
        consodata += "(" + idbut + "," + i_compte + "," + stack[i_compte] + ")";
    }
    var post_data = "consodata=" + encodeURIComponent(consodata);
    xhr.send(post_data);
}

/* Fonction qui fait des consos avec plusieurs notes et un plusieurs boutons */
function do_conso_multiples() {
    // On se prépare à poster une requête
    var xhr = getXMLHttpRequest();
    // On définit ce qu'on fera quand on recevra la réponse = gestion de l'éventuelle erreur
    xhr.onreadystatechange = function() {
        if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0)) {
            if (xhr.responseText=='"Erreur"') {
            // Si on reçoit une erreur, on sauvegarde les stacks et on redirige
            // Ici, il faut également simplifier les stacks pour qu'il rentre dans les cookies
                for (var note in display_stack) {
                    delete display_stack[note]["source"]
                }
                for (var note in display_stack_2) {
                    delete display_stack[note]["source"]
                }
                if (!isEmpty(stack_button)) { // Soit on faisait des consos
                saveStack("stack_button",stack_button)
                saveStack("ordered_stack",ordered_stack)
                }
                else { // Soit on faisait des transfertd
                saveStack("display_stack_2",display_stack_2)
                saveStack("cstack_2",stack_2)
                }
                saveStack("display_stack",display_stack)
                saveStack("cstack",stack)


                window.location.replace(gotoURI("consos-double"));
                return;
            }
            var answer = JSON.parse(xhr.responseText);
            if (!isSuccessCode(answer["retcode"])) {
                display_error(answer["errmsg"]);
                return;
            }
            answer = answer["msg"];
            // Si il y a eu des échecs on alerte
            var errorhappened = false;
            var errlist = [];
            for (var i_ans in answer) {
                ans = answer[i_ans];
                if (!isSuccessCode(ans[0])) {
                    errorhappened = true;
                    // Il faut trouver les noms de note correspondants
                    var nom = "";
                    for (var i_note in display_stack) {
                        note = display_stack[i_note];
                        if (note["idbde"] == ans[1][1]) {
                            if (nom != "") {
                                nom += " aka ";
                            }
                            nom += note["affiche"];
                        }
                    }
                    // et le bouton concerné
                    var bouton = stack_button[ans[1][0]];
                    errlist.push(nom + ", " + bouton["nb"] + "x" + bouton["label"] + " : " + ans[2]);
                }
            }
            // On vide tous les stacks, parce que garder des transactions échouées n'aurait pas de sens ici
            // stack & stack_button
            emptyStack(false, true);
            if (errorhappened) {
                display_error("Attention :", errlist);
            }
            // et on rafraîchit les stacks
            refreshStack();
            refreshStack(true);
            // et l'historique
            refreshHistorique();
        }
    };
    // On fabrique la requête en parsant les deux stacks
    xhr.open("POST", NOTE_ROOT_URL + "do_conso/", true);
     xhr.setRequestHeader("X-CSRFToken", csrftoken);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    var consodata = ""
    for (var i_button in stack_button) {
        for (var i_compte in stack) {
            if (consodata != "") {
                consodata += ",";
            }
            consodata += "(" + i_button + "," + i_compte + "," + stack[i_compte]*stack_button[i_button]["nb"] + ")";
        }
    }
    var post_data = "consodata=" + encodeURIComponent(consodata);
    xhr.send(post_data);
}

/* Fonction de mise à jour de l'historique */
function refreshHistorique() {
    // $ = c'est du jQuery
    $("#historique").load(NOTE_ROOT_URL + "consos/ #historique");
    $('#id_search_field').val("");
    $('#liste_notes').empty();
    $('#id_search_field_2').val("");
    $('#liste_notes_2').empty();
}


/*** Les affichages mouseover ***/
/* Affichage d'un bouton */
function displayButton(label, montant, description, consigne) {
    /* montant est une variable texte déjà formatée */
    var old_displayer = document.getElementById("current_selection");
    var new_displayer = document.createElement("p");
    new_displayer.setAttribute("id", "current_selection");
    new_displayer.setAttribute("class", "current_selection");
    new_displayer.appendChild(document.createTextNode(label));
    new_displayer.appendChild(document.createElement("br"));
    new_displayer.appendChild(document.createTextNode(montant));
    new_displayer.appendChild(document.createElement("br"));
    new_displayer.appendChild(document.createTextNode(description));
    if (consigne) {
        var consigne = document.createElement("img");
        consigne.setAttribute("src", STATIC_URL + "/img/recyclable.png");
        consigne.setAttribute("alt", "consigné");
        new_displayer.appendChild(document.createElement("br"));
        new_displayer.appendChild(consigne);
    }
    old_displayer.parentNode.replaceChild(new_displayer, old_displayer);
}

/* Affichage d'un compte (avec son éventuelle photo) */
function displayAccount(objetnote, note, idbde, solde, timenegatif, aka) {
    /* solde est une variable texte déjà formatée */
    var xhr = getXMLHttpRequest();
    xhr.onreadystatechange = function() {
        if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0)) {
            /* Une fois la photo chargée, on peut l'inclure dans la page.
               Au lieu de faire du innerHTML dégueulasse, on va jouer à
               être intelligent et utiliser la bonne adresse pour la photo */
            var old_displayer = document.getElementById("current_selection");
            var new_displayer = document.createElement("p");
            new_displayer.setAttribute("id", "current_selection");
            var photo = document.createElement("img");
            photo.setAttribute("src", NOTE_ROOT_URL + "media/photos/" + idbde + ".png");
            var photo_linked = document.createElement("a");
            photo_linked.setAttribute("href", NOTE_ROOT_URL + "comptes/" + idbde + "/");
            photo_linked.appendChild(photo);
            new_displayer.appendChild(photo_linked);
            new_displayer.appendChild(document.createElement("br"));
            var objnote = objetnote.cloneNode(true);
            objnote.appendChild(document.createElement("br"));
            if (aka) { // Quand c'est un ancien pseudo ou un alias, on affiche le vrai pseudo
                objnote.appendChild(document.createTextNode("aka " + aka));
                objnote.appendChild(document.createElement("br"));
            }
            objnote.appendChild(document.createTextNode("" + solde/100 + " €"));
            if (solde<0) {
                objnote.appendChild(document.createElement("br"));
        if (timenegatif == 'n') {objnote.appendChild(document.createTextNode(gettext("depuis ") + timenegatif + gettext(" jours")));}
        else if (timenegatif > 1) {objnote.appendChild(document.createTextNode(gettext("depuis ") + timenegatif + gettext(" jours")));}
        else {objnote.appendChild(document.createTextNode(gettext("depuis ") + timenegatif + gettext(" jour")));}
            }
            new_displayer.appendChild(objnote);
            old_displayer.parentNode.replaceChild(new_displayer, old_displayer);
        }
    };
    // On fabrique la requête en utilisant l'idbut et en parsant stack
    xhr.open("POST", NOTE_ROOT_URL + "get_photo/" + idbde + "/", true);
     xhr.setRequestHeader("X-CSRFToken", csrftoken);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xhr.send(null);
}


/*** Crédits et Retraits et Transferts ***/
/* Fonction qui va chercher le contenu des champs du formulaire et envoie le crédit/retrait */
function crediter_ou_retirer(credit) {
    if (credit) {
        var target = 'credit';
    }
    else {
        var target = 'retrait';
    }
    var idbde_field = document.getElementById("id_" + target + "_form-idbde");
    var montant_field = document.getElementById("id_" + target + "_form-montant");
    var commentaire_field = document.getElementById("id_" + target + "_form-commentaire");
    var type_field = document.getElementById("id_" + target + "_form-type");
    var nom_field = document.getElementById("id_" + target + "_form-nom");
    var prenom_field = document.getElementById("id_" + target + "_form-prenom");
    var banque_field = document.getElementById("id_" + target + "_form-banque");
    
    /* On envoie la requête grâce à jQuery */
    jQuery.ajax({
        url: NOTE_ROOT_URL + "do_" + target + "/",
        type: "POST",
        data: ({idbde: idbde_field.value,
                montant: montant_field.value,
                commentaire: commentaire_field.value,
                type: type_field.value,
                nom: nom_field.value,
                prenom: prenom_field.value,
                banque: banque_field.value}),
        dataType: "text",
        success:
            function(msg) {
                /* On traite les éventuelles erreurs */
                try {
                    var answer = JSON.parse(msg);
                    var cod = answer["retcode"];
                }
                catch(err) {
                    display_error(msg);
                    return;
                }
                if (isSuccessCode(cod)) {
                    cleanForms();
                    emptyStack();
                    refreshHistorique();
                }
                else {
                    display_error(answer["errmsg"]);
                }
            }
        }
    );
}

/* Fonction qui va chercher le contenu des champs du formulaire et les emetteurs/destinataires et envoie le transfert
   La fonction appelée pour récupérer les listes des émetteurs et des destinataires est différente en mode double stack */
function transferer() {
    var montant_field = document.getElementById("id_transfert_form-montant");
    var commentaire_field = document.getElementById("id_transfert_form-commentaire");
    var ok, emetteurs, destinataires, temp;
    // On fait appel à la fonction qui récupère émetteurs et destinataires différemment selon le stack_mode
    if (double_stack_mode) {temp = transfert_get_people_double();}
    else {temp = transfert_get_people_single();}
    ok = temp[0]
    // En mode single stack, on doit vérifier avant qu'on a bien un émetteur et un destinataire.
    if (!ok) {
        display_error(temp[1]);
        return;
    }
    emetteurs = temp[1];
    destinataires = temp[2];
    
    // On se prépare à poster une requête
    var xhr = getXMLHttpRequest();
    // On définit ce qu'on fera quand on recevra la réponse = gestion de l'éventuelle erreur
    xhr.onreadystatechange = function() {
        if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0)) {
            if (xhr.responseText=='"Erreur"') {
                // Si on reçoit une erreur, on sauvegarde les stacks et on redirige
                // Ici, il faut également simplifier les stacks pour qu'il rentre dans les cookies
                for (var note in display_stack) {
                    delete display_stack[note]["source"]
                }
                saveStack("display_stack",display_stack)
                saveStack("cstack",stack)
                // On sauvegarde le deuxième stack que en mode doublestack
                if (double_stack_mode) {
                    for (var note in display_stack_2) {
                        delete display_stack_2[note]["source"]
                    }
                    saveStack("display_stack_2",display_stack_2)
                    saveStack("cstack_2",stack_2)
                }
                window.location.replace(gotoURI("consos-double"));
                return;
            }
            var answer = JSON.parse(xhr.responseText);
            if (!isSuccessCode(answer["retcode"])) {
                display_error(answer["errmsg"], null, answer["retcode"] == 666);
                return;
            }
            answer = answer["msg"];
            // Si il y a eu des échecs on alerte
            var errorhappened = false;
            var errlist = [];
            for (var i_ans in answer) {
                ans = answer[i_ans];
                if (!isSuccessCode(ans[0])) {
                    errorhappened = true;
                    // Il faut trouver les noms de note correspondants
                    var nom = "";
                    var nomdest = "";
                    // Selon si on est en double_stack ou pas, le num du destinataire ne se trouve pas au même endroit
                    if (double_stack_mode) {
                        for (var i_note in display_stack) {
                            note = display_stack[i_note];
                            if (note["idbde"] == ans[1][0]) {
                                if (nom != "") {
                                    nom += " aka ";
                                }
                                nom += note["affiche"];
                            }
                        }
                        var nomdest = "";
                        for (var i_note in display_stack_2) {
                            note = display_stack_2[i_note];
                            if (note["idbde"] == ans[1][1]) {
                                if (nomdest != "") {
                                    nomdest += " aka ";
                                }
                                nomdest += note["affiche"];
                            }
                        }
                    }
                    else {
                        for (var i_note in display_stack) {
                            note = display_stack[i_note];
                            if (note["idbde"] == ans[1][0]) {
                                if (nom != "") {
                                    nom += " aka ";
                                }
                                nom += note["affiche"];
                            }
                            if (note["idbde"] == ans[1][1]) {
                                if (nomdest != "") {
                                    nomdest += " aka ";
                                }
                                nomdest += note["affiche"];
                            }
                        }
                    }
                    // et le bouton concerné
                    errlist.push("De " + nom + " à " + nomdest + " : " + ans[2]);
                }
            }
            // En conso double, on vide tout les stacks
            if (double_stack_mode) {
                refreshStack();
                emptyStack(false);
                emptyStack(true);
                refreshStack(true);
                cleanForms();
            }
            else {
                if (size(stack) == 2) {
                    erase_transfert_note(true);
                    erase_transfert_note(false);
                    emptyStack(false);
                    refreshStack();
                    cleanForms();
                }
                else {
                    var note = get_last_item(display_stack);
                    remove_note(display_stack[note]["affiche"], display_stack[note]["idbde"], false);
                    refreshStack();
                    var note = display_stack[get_last_item(display_stack)];
                    // On remplit d'abord le TO si il est vide
                    if ((transfert_to == null) && (transfert_from != note["idbde"])) {
                        var note_destinataire = document.getElementById("transfert_destinataire");
                        note_destinataire.replaceChild(note["source"].cloneNode(true), note_destinataire.firstChild);
                        transfert_to = note["idbde"];
                    }
                    else {
                        // Si il contient déjà quelque chose, alors on remplit le FROM
                        if (transfert_from == null) {
                            var note_emetteur = document.getElementById("transfert_emetteur");
                            note_emetteur.replaceChild(note["source"].cloneNode(true), note_emetteur.firstChild);
                            transfert_from = note["idbde"];
                        }
                    }
                }
            }
            if (errorhappened) {
                display_error("Attention :", errlist);
            }
            // on met à jour l'historique
            refreshHistorique();
        }
    };
    xhr.open("POST", NOTE_ROOT_URL + "do_transfert/", true);
    xhr.setRequestHeader("X-CSRFToken", csrftoken);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    var transfertdata = [page_dons, emetteurs, destinataires, montant_field.value, commentaire_field.value];
    transfertdata = JSON.stringify(transfertdata);
    var post_data = "transfertdata=" + encodeURIComponent(transfertdata);
    xhr.send(post_data);
}

/* Fonction pour afficher une erreur de transaction dans le div prévu à cet effet */
function display_error(text, error_list, is_traceback) {
    // text est affichée, puis les élements de l'error_list en <ul>
    var errdiv = document.getElementById("messages_div");
    // on crée le div qu'on va rajouter
    var alertdiv = document.createElement("div");
    alertdiv.setAttribute("class", "alert alert-danger alert-dismissible"); // les échecs de transaction sont toujours des erreurs
    // on crée aussi la croix pour le fermer
    var croix = document.createElement("button");
    croix.setAttribute("class", "close");
    croix.setAttribute("data-dismiss", "alert");
    croix.setAttribute("href", "#");
    croix_span = document.createElement("span");
    croix_span.setAttribute("aria-hidden", "true");
    croix_span.appendChild(document.createTextNode("\u00D7"));
    croix.appendChild(croix_span);
    alertdiv.appendChild(croix);
    if (text != "") {
        if (is_traceback) {
            var err_container = document.createElement("pre");
        } else {
            var err_container = document.createElement("p");
        }
        err_container.appendChild(document.createTextNode(text));
        alertdiv.appendChild(err_container);
    }
    if (error_list) {
        var liste = document.createElement("ul");
        var elem;
        for (var i = 0; i < error_list.length; i++) {
            elem = document.createElement("li");
            elem.appendChild(document.createTextNode(error_list[i]));
            liste.appendChild(elem)
        };
        alertdiv.appendChild(liste);
    }
    errdiv.appendChild(alertdiv);
}

function get_last_item(obj) {
var k = 0, sze= size(obj);
for (var note in obj) {
k++;
if (k == sze) {return note}
}
}

function size(obj) {
var size = 0, key;
for (key in obj) {
if (obj.hasOwnProperty(key)) size++;
}
return size;
}
