# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2016-06-15 16:45-0300\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: boutons_dynamiques.js:157
msgid "Label"
msgstr "Label"

#: boutons_dynamiques.js:157
msgid "Montant"
msgstr "Amount"

#: boutons_dynamiques.js:157
msgid "Destinataire"
msgstr "Receiver"

#: boutons_dynamiques.js:157
msgid "Catégorie"
msgstr "Category"

#: boutons_dynamiques.js:157
msgid "Affiché"
msgstr "Visible"

#: boutons_dynamiques.js:157
msgid "Description"
msgstr "Description"

#: boutons_dynamiques.js:157
msgid "Consigné"
msgstr "Returnable"

#: boutons_dynamiques.js:157 boutons_dynamiques.js:179
msgid "Modifier"
msgstr "Edit"

#: boutons_dynamiques.js:157 boutons_dynamiques.js:182
msgid "Supprimer"
msgstr "Delete"

#: boutons_dynamiques.js:185 boutons_dynamiques.js:193
msgid "Oui"
msgstr "Yes"

#: boutons_dynamiques.js:189 boutons_dynamiques.js:196
msgid "Non"
msgstr "No"

#: boutons_dynamiques.js:218
msgid "Aucun bouton ne correspond à ta recherche."
msgstr "No button match your search."

#: consos_base.js:766 consos_base.js:767 consos_base.js:768
msgid "depuis "
msgstr "since "

#: consos_base.js:766 consos_base.js:767
msgid " jours"
msgstr " days"

#: consos_base.js:768
msgid " jour"
msgstr " day"

#: liste_dynamique_search.js:148
msgid "idbde"
msgstr "idbde"

#: liste_dynamique_search.js:148
msgid "Nom"
msgstr "Last name"

#: liste_dynamique_search.js:148
msgid "Prénom"
msgstr "First name"

#: liste_dynamique_search.js:148
msgid "Pseudo"
msgstr "Nickname"

#: liste_dynamique_search.js:148
msgid "Aliases"
msgstr "Aliases"

#: liste_dynamique_search.js:148
msgid "Anciens pseudos"
msgstr "Formers nicknames"

#: liste_dynamique_search.js:148
msgid "E-mail"
msgstr "Email"

#: liste_dynamique_search.js:148
msgid "Solde"
msgstr "Balance"

#: liste_dynamique_search.js:148
msgid "Section"
msgstr "Departement"

#: liste_dynamique_search.js:196
msgid "Résultats de la recherche"
msgstr "Search results"

#: liste_dynamique_search.js:209
msgid "Aucun compte ne correspond à ta recherche."
msgstr "No account match your search."

#: trad.js:1
msgid "Alcool"
msgstr "Alcohol"

#: trad.js:2
msgid "Autre"
msgstr "Other"

#: trad.js:3
msgid "Bouffe"
msgstr "Food"

#: trad.js:4
msgid "Pulls"
msgstr "Hoodies"

#: trad.js:5
msgid "Soft"
msgstr "Soft Drink"
