/*******************************************************************************
                    Script consos en mode double stack
*******************************************************************************/

var double_stack_mode = false;

var stack = {};
var stack_2 = {};

var display_stack = {};
var display_stack_2 = {};

var stack_button = {};

function transfert_get_people_double() {
    var emetteurs = [];
    var destinataires = [];
    //On remplit la liste des emetteurs avec le stack
    for (var i_emet in stack) {
        for (var i = 0; i < stack[i_emet]; i++) {
            //On le place autant de fois que son coefficient
            emetteurs.push(parseInt(i_emet));
        }
    }
    //On remplit la liste des destinataires avec le stack_2
    for (var i_dest in stack_2) {
        for (var i = 0; i < stack_2[i_dest]; i++) {
            //On le place autant de fois que son coefficient
            destinataires.push(parseInt(i_dest));
        }
    }
    return [true, emetteurs, destinataires];
}
