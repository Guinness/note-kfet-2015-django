/* Script JS qui permet d'afficher une liste dynamique
   des notes en faisant un search */

/** Il intéragit avec 3 éléments de la page, repérés par leur id :
 - "id_search_field" : l'élément dans lequel on récupère le terme recherche
                       et aussi où sera placé le pseudo/alias/… quand on clique sur une note
 - "liste_comptes" : l'élément (tableau) qui sera remplacé par la liste des notes obtenue
**/

/* fonctions de hl et de cliquabilité des lignes du tableau de recherche */
var readhesion = false;
function ChangeColor(tableRow, highLight)
{
    if (highLight)
    {
        tableRow.style.backgroundColor = '#dcdcdc';
    }
    else
    {
        tableRow.style.backgroundColor = 'white';
    }
}

function GoTo(url)
{
    window.location = url;
}

/* fonction qui effectue la recherche puis appelle readData en callback */
function request(asked, callback) {
    var xhr = getXMLHttpRequest();
    
    xhr.onreadystatechange = function() {
        if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0)) {
            // la fonction de callback a besoin aussi de la question
            callback(xhr.responseText, asked);
        }
    };

    /* On envoie la requête en POST */
    xhr.open("POST", NOTE_ROOT_URL + "get_boutons/a/", true);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xhr.send("asked=" + encodeURIComponent(asked));
}

/* fonction appelée à la fin du timer */
function getInfo() {
    var search_field = document.getElementById("id_search_field");
    var asked = search_field.value;
    /* on ne fait la requête que si on a au moins un caractère pour chercher */
    if (asked.length >= 1) {
        request(asked, readDataNormal);
    }
}

var timer;
var timer_on;
/* Fontion appelée quand le texte change (délenche le timer) */
function search_field_moved(secondfield) {
    if (timer_on) { // Si le timer a déjà été lancé, on réinitialise le compteur.
        clearTimeout(timer);
        timer = setTimeout("getInfo(" + secondfield + ")", 300);
    }
    else { // Sinon, on le lance et on enregistre le fait qu'il tourne.
        timer = setTimeout("getInfo(" + secondfield + ")", 300);
        timer_on = true;
    }
}

/* fonction de traitement des champs des résultats
   Ajoute le texte dans une case td à la ligne.
   Highlighte le résultat recherché */

// On ne recherche que sur ces champs
var highlight_fields = ["label", "categorie", "description"];

function createField(table_line, content, type, highlight, colonne) {
    var highlight = typeof highlight !== 'undefined' ? highlight : null;
    var colonne = typeof colonne !== 'undefined' ? colonne : null;

    var caze = document.createElement(type);
    if (content == null) {
        content = "";
    }
    if (content.tagName === "A") {
        caze.appendChild(content);
    }
    else if ((highlight==null) || (highlight_fields.indexOf(colonne) == -1)) {
        caze.appendChild(document.createTextNode(content));
    }
    else {
        var r = new RegExp(highlight, "ig");
        var match, mark;
        var indexes = [];
        while (match = r.exec(content)) {
            indexes.push([match.index, match.index + match[0].length]);
        }
        if (indexes.length == 0) {
            caze.appendChild(document.createTextNode(content));
        }
        else {
            caze.appendChild(document.createTextNode(content.slice(0, indexes[0][0])));
            indexes.push([content.length, null]);
            for (var i = 0; i < indexes.length - 1; i ++) {
                mark = document.createElement("mark");
                mark.appendChild(document.createTextNode(content.slice(indexes[i][0], indexes[i][1])));
                caze.appendChild(mark);
                caze.appendChild(document.createTextNode(content.slice(indexes[i][1], indexes[i + 1][0])));
            }
        }
    }

    // On masque certaines colonnes en fonction du viewport (Bootstrap 3 requis)
    hidden_xs = new Array('destinatairepseudo', 'categorie', 'affiche', 'description', 'consigne')
    hidden_sm = new Array('affiche', 'description', 'consigne')

    if (type === "th") {
        caze.className += "text-center";
    }

    if (hidden_xs.indexOf(colonne) != -1) {
        caze.className += " hidden-xs";
    }

    if (hidden_sm.indexOf(colonne) != -1) {
        caze.className += " hidden-sm";
    }

    table_line.appendChild(caze);
}

function createButton(text, url, button_class) {
    // On fabrique les boutons modifier
    bouton = document.createElement("a");
    bouton.href = url;
    bouton.className = button_class;
    bouton.textContent = text;

    return bouton;
}

/* wrapper pour readData */
function readDataNormal(oData, asked) {
    return readData(oData, asked);
}

/* fonction qui traite les données à leur retour */
function readData(oData, asked) {
    var gotlist = JSON.parse(oData);
    var liste = document.createElement("table");
    liste.setAttribute("class", "table table-striped");
    // on crée la ligne de titre

    var thead = document.createElement("thead");
    var first_line = document.createElement("tr");
    thead.appendChild(first_line);
    var th_field_list = [gettext("Label"), gettext("Montant"), gettext("Destinataire"), gettext("Catégorie"), gettext("Affiché"), gettext("Description"), gettext("Consigné"), gettext("Modifier"), gettext("Supprimer")];
    var field_list = ["label", "montant", "destinatairepseudo", "categorie", "affiche", "description", "consigne", "modifier", "supprimer"];
    var colonne, valcolonne;
    for (var icol = 0; icol < field_list.length; icol++) {
        colonne = field_list[icol];
        content = gettext(th_field_list[icol]);
        createField(first_line, content, "th", null, colonne);
    }
    var ligne;
    var size = gotlist.length;
    if (size>0)
    {
        liste.appendChild(thead);
    }
    var tbody = document.createElement("tbody");
    for (var i = 0, c = size; i<c; i++) {
        // on crée une nouvelle ligne
        ligne = document.createElement("tr");
        ligne.className = "text-center"
        // on la remplit avec les champs du bouton
        gotlist[i]["montant"] = "" + (gotlist[i]["montant"]/100) + " €";
        // On fabrique les boutons modifier
        modifier_bouton = createButton(gettext("Modifier"), NOTE_ROOT_URL + 'boutons/' + gotlist[i]["id"] + '/', "btn btn-primary btn-small");
        gotlist[i]["modifier"] = modifier_bouton
        // Puis les boutons supprimer
        suppr_bouton = createButton(gettext("Supprimer"), NOTE_ROOT_URL + 'boutons/' + gotlist[i]["id"] + '/delete/', "btn btn-danger btn-small");
        gotlist[i]["supprimer"] = suppr_bouton
        if(gotlist[i]["affiche"] === true) {
            gotlist[i]["affiche"] = gettext("Oui");
            ligne.className += " info";
        }
        else {
            gotlist[i]["affiche"] = gettext("Non");
            ligne.className += " danger";
        }
        if(gotlist[i]["consigne"] === true) {
            gotlist[i]["consigne"] = gettext("Oui");
        }
        else {
            gotlist[i]["consigne"] = gettext("Non");
        }
        for (var icol = 0; icol < field_list.length; icol++) {
            colonne = field_list[icol];
            valcolonne = gettext(gotlist[i][colonne]);

            createField(ligne, valcolonne, "td", asked, colonne);
        }
        // on rend la ligne cliquable
        ligne.addEventListener("click", GoTo.bind("trapped", NOTE_ROOT_URL + "boutons/" + gotlist[i]["id"] + "/"));

        // on ajoute la ligne au tableau
        tbody.appendChild(ligne);
    }
    liste.appendChild(tbody);
    old_liste = document.getElementById("liste_dynamique");
    // on lui donne le même id qu'avant pour pouvoir recommencer
    liste.setAttribute("id", "liste_dynamique");
    liste.className = "table table-condensed table-bordered";
    if (size <= 0)
    {
        var no_match = document.createElement("p");
        var no_match_text = document.createTextNode(gettext("Aucun bouton ne correspond à ta recherche."));
        no_match.id = "liste_dynamique";
        no_match.className = "alert alert-warning lead";
        no_match.appendChild(no_match_text);
        liste = no_match;
    }

    old_liste.parentNode.replaceChild(liste, old_liste);
}
