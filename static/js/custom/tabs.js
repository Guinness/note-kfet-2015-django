var currentTab='#conso';
var was_conso_double = false;
$('li a').click(function(e){
    if (currentTab == '#transfert' && !was_conso_double) { //si on était sur transfert et qu'avant on était en conso-simple, on y retourne
        switch_conso_mode(false);
    }
    //Lors du clic sur un lien, on affiche l'onglet concerné
    $('li a, .active').removeClass('active');
    $(this.parentNode).addClass('active');
    currentTab = $(this).attr('href');
    $(currentTab).addClass('active');
    setActiveAppName(); //On améliore la responsiveness des onglets pour les mobiles

    if (['#transfert', '#retrait', '#credit'].indexOf(currentTab) == -1) {
        $('#config_button').removeClass('hidden');
    }
    else {
        $('#config_button').addClass('hidden');
    }

    //Puis on s'occupe des mises en formes spéciales
    if (currentTab == '#transfert') {
        was_conso_double = double_stack_mode;
        switch_conso_mode(true);
        $('#conso-double-btn').addClass('hidden');
        if (!isEmpty(stack_button)) {
            //Il y avait des boutons dans le stack_button, mais on va utiliser le stack pour des notes. On le vide.
            emptyStack(false, true);
        }
        $('.transfert_conso_double').removeClass('hidden');
        if(double_stack_mode){
            $('.transfert_conso_double').addClass('hidden');
            $('#conso-double-btn').addClass('hidden');
            $('#stack_container_2, #search_2').removeClass('hidden');
        }
    }
    else if (currentTab == '#credit') {
        $('#stack_container_2, #search_2, #conso-double-btn').addClass('hidden');
        $('#stack_label').addClass('hidden');
        if (!isEmpty(stack_button)) {
            //Il y avait des boutons dans le stack_button, mais on va utiliser le stack pour des notes. On le vide.
            emptyStack(false, true);
        }
    }
    else if (currentTab == '#retrait') {
        $('#stack_container_2, #search_2, #conso-double-btn').addClass('hidden');
        $('#stack_label').addClass('hidden');
        if (!isEmpty(stack_button)) {
            //Il y avait des boutons dans le stack_button, mais on va utiliser le stack pour des notes. On le vide.
            emptyStack(false, true);
        }
    }
    else
    {
        $('#stack_label_2').addClass('hidden');
        $('#stack_label').addClass('hidden');
        if(double_stack_mode){
            $('#stack_container_2, #conso-double-btn').removeClass('hidden');
            $(' #search_2').addClass('hidden');
        }
        else
        {
            $('#stack_container_2, #search_2, #conso-double-btn').addClass('hidden');
        }
    }
    e.preventDefault();

});



$('#id_search_field').keypress(function(e) {
    if(e.which == 13) {
  var prout =$( "#liste_notes li span" ).first().trigger( "click");
    $('#id_search_field').val("");
    }
});

$('#id_search_field').click(function() {
    $('#id_search_field').val("");
    $('#liste_notes').empty();
});

$('#id_search_field_2').keypress(function(e) {
    if(e.which == 13) {
  var prout =$( "#liste_notes_2 li span" ).first().trigger( "click");
    $('#id_search_field_2').val("");
    }
});

$('#id_search_field_2').click(function() {
    $('#id_search_field_2').val("");
    $('#liste_notes_2').empty();
});


function switch_conso_mode(conso_double_wanted){
if (conso_double_wanted && !double_stack_mode) 
{
    $('#tab-content').removeClass('col-lg-7');
    $('#tab-content').addClass('col-lg-5');
if (!(currentTab == '#retrait' || currentTab == '#credit')) {
        $('#stack_container_2, #conso-double-btn').removeClass('hidden');
        if (!isEmpty(stack_button)) {
            //Il y avait des boutons dans le stack_button, mais on va utiliser le stack pour des notes. On le vide.
            emptyStack(false, true);
        }
    if (currentTab == '#transfert') {
        $('#conso-double-btn').addClass('hidden');
        $('.transfert_conso_double').addClass('hidden');
        $('#search_2').removeClass('hidden');
        if (!isEmpty(stack_button)) {
            //Il y avait des boutons dans le stack_button, mais on va utiliser le stack pour des notes. On le vide.
            emptyStack(false, true);
        }
    }
    }
    double_stack_mode =true;
    $('#conso-simple').css({"color": "#333333", "font-weight": "normal" });
    $('#conso-double').css({"color": "#000", "font-weight": "bold" });
}

if (!conso_double_wanted && double_stack_mode){
    $('#tab-content').removeClass('col-lg-5');
    $('#tab-content').addClass('col-lg-7');
    if (!(currentTab == '#retrait' || currentTab == '#credit')) {
        $('#stack_container_2, #conso-double-btn').addClass('hidden');
    }
    if (currentTab == '#transfert') {
        $('.transfert_conso_double').removeClass('hidden');
        $('#search_2').addClass('hidden');
    }
    double_stack_mode =false;
    $('#conso-double').css({"color": "#333333", "font-weight": "normal" });
    $('#conso-simple').css({"color": "#000", "font-weight": "bold" });
}


}


$('#conso-double').click(function(e){
    switch_conso_mode(true)
});



$('#conso-simple').click(function(e){
    switch_conso_mode(false)
});

/* Amélioration de la responsiveness de l'app Consos */

function reorganizeResponsively() {
    // On teste si on se trouve su un appareil mobile ou non
    // Hypothèse induite par Bootstrap: mobile <=> width < 768

    var tab_content = $('#tab-content');

    if ($(window).width() < 768) {
        $('#tab-content').remove();
        $('#tab-frame').prepend(tab_content);

        // Sur mobile, on force le mode consos double
        // et on force le deuxième champ de recherche à apparaître
        switch_conso_mode(true);
        if (currentTab != '#transfert') {
            $('#search_2').removeClass('hidden');
        }

        // Si on était sur pas sur transfert/retrait/crédit, on passe
        // sur le mode Consos
        if (['#transfert', '#credit', '#retrait'].indexOf(currentTab) == -1) {
            $('li.active').removeClass('active');
            $('#consos_responsive').addClass('active');
            setActiveAppName();
        }
    }
    else {
        $('#tab-content').remove();
        $('#tab-frame').append(tab_content);

        if (currentTab != '#transfert') {
            $('#search_2').addClass('hidden');
        }

        if (['#transfert', '#credit', '#retrait'].indexOf(currentTab) == -1) {
            if (currentTab == '#conso') {
                $('#consos_reference').addClass('active');
            }
            else {
                $(currentTab).parent().addClass('active');
            }
            $('#consos_reponsive').removeClass('active');
        }
    }
}

$(window).resize(reorganizeResponsively);

reorganizeResponsively();
