/* Script JS qui permet d'afficher une liste dynamique
   des notes en faisant un quick_search */

/** Il intéragit avec les éléments de la page.

 Repérés par leur id :
 - "liste_notes" : l'élément (liste) qui sera remplacé par la liste des notes obtenue
 
 Quand les données XMLHttpRequest sont reçues, il faut appeler dessus la fonction readData
  en ajoutant un paramètre booléen.
 - true si on veut que les notes appellent la fonction displayAccount sur un onMouseOver
 - false sinon
 
 Quand on clique sur une note, la fontion put_note est appelé avec 6 paramètres :
 - l'objet sur lequel on a cliqué
 - le texte qu'il contient
 - l'idbde du compte correspondant
 - le nom du compte correspondant
 - le prénom du compte correspondant
 - tosecondstack
**/

var href = window.location.href;
var tmpArray = href.split('/');
var pagename = tmpArray[tmpArray.length - 2];
// pagename est le nom de la page courante

/* Fonction qui prépare vers où on est redirigé, avec le ?next */
function gotoURI(go) {
    return NOTE_ROOT_URL + "?next=" + encodeURI(NOTE_ROOT_URL + go + "/");
}

/* Fonction qui effectue la recherche d'un type de données puis appelle readData en callback */
function request(type, asked, callback) {
    var xhr = getXMLHttpRequest();
    
    xhr.onreadystatechange = function() {
        if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0)) {
            if (xhr.responseText == '"Erreur"') {
                if (pagename == 'consos'){
                    if (!isEmpty(stack_button)) { //Soit on faisait des consos
                        saveStack("stack_button",stack_button)
                        saveStack("ordered_stack",ordered_stack)
                    }
                    if (!isEmpty(display_stack)) { //Soit on faisait des consos
                        for (var note in display_stack) {
                            delete display_stack[note]["source"]
                        }
                        saveStack("display_stack",display_stack)
                        saveStack("cstack",stack)
                    }
                }
                if (pagename == 'consos-double'){
                    if (!isEmpty(stack_button)) { //Soit on faisait des consos
                        saveStack("stack_button",stack_button)
                        saveStack("ordered_stack",ordered_stack)
                    }
                    if (!isEmpty(display_stack)) { //Soit on faisait des consos
                        for (var note in display_stack) {
                            delete display_stack[note]["source"]
                        }
                        saveStack("display_stack",display_stack)
                        saveStack("cstack",stack)
                    }
                    if (!isEmpty(display_stack_2)) { //Soit on faisait des consos
                        for (var note in display_stack_2) {
                            delete display_stack_2[note]["source"]
                        }
                        saveStack("display_stack_2",display_stack_2)
                        saveStack("cstack_2",stack_2)
                    }
                }

                window.location.replace(gotoURI(pagename));
                return;
            }
            callback(xhr.responseText);
        }
    };
    /* On envoie la requête en POST */
    if (page_dons) {
        target = "quick_search_dons/";
    } else if (type == "bouton") {
        target = "get_boutons/b/"
    } else {
        target = "quick_search_basic/";
    }
    xhr.open("POST", NOTE_ROOT_URL + target, true);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xhr.setRequestHeader("X-CSRFToken", csrftoken);
    xhr.send("asked=" + encodeURIComponent(asked));
}

var timer;
var timer_on;
/* Fontion appelée quand le texte change (délenche le timer) */
function search_field_moved(secondfield) {
    var type;
    if ((typeof currentTab != 'undefined') && (["#transfert", "#retrait", "#credit"].indexOf(currentTab) == -1) && secondfield) {
        type = "bouton"
    } else {
        type = "note"
    };
    if (timer_on) { // Si le timer a déjà été lancé, on réinitialise le compteur.
        clearTimeout(timer);
        timer = setTimeout("getInfo(" + secondfield + ",'" + type + "')", 100);
    }
    else { // Sinon, on le lance et on enregistre le fait qu'il tourne.
        timer = setTimeout("getInfo(" + secondfield + ",'" + type + "')", 100);
        timer_on = true;
    }
}

/* Fonction qui traite les données à leur retour
   (elle est wrappée par les scripts appelants pour lui fournir les paramètre suivants) */
function readData(oData, targetid, display, tosecondstack) {
    var gotlist = JSON.parse(oData);
    var liste = document.createElement("ul");
    liste.setAttribute("class", "liste_notes");
    var rep, elmt, texte, elemt_span, class1, class2, class3, aka, tempfunction;

    var buttonList = (typeof currentTab != 'undefined') && (["#transfert", "#retrait", "#credit"].indexOf(currentTab) == -1) && tosecondstack;

    for (var i = 0, c = gotlist.length; i < c; i++) {
        rep = gotlist[i];
        // on crée un nouvel élément
        elemt = document.createElement("li");
        if (!buttonList) {
            texte = document.createTextNode(rep["terme"]);
        } else {
            texte = document.createTextNode(rep["label"] + " (" + rep["categorie"] + ")");
        }
        // on fait de la mise en forme à coup de spans
        elemt_span = document.createElement("span");
        // on le met en forme en fonction de ce que c'est comme terme de recherche
        if (!buttonList) {
            aka = rep["pseudo"];
            if (rep["was"] == "pseudo") {
                class1 = "liste_pseudo";
                aka = null; // Si le compte a été trouvé par son pseudo, on n'a pas besoin de garder le aka qui est redondant
            }
            if (rep["was"] == "historique") {
                class1 = "liste_historique";
            }
            if (rep["was"] == "alias") {
                class1 = "liste_alias";
            }
            if (rep["was"] == "idbde") {
                class1 = "liste_idbde";
            }
            if (rep["vieux"]) {
                class3 = "liste_potvieux";
            } else {
                class3 = "";
            }
            // on le met aussi en forme en fonction de si il est negatif ou pas
            class2 = "liste_negatif" + rep["negatif"]
            // on donne les deux classes à l'élément span
            elemt_span.setAttribute("class", "liste " + class1 + " " + class2 + " " + class3);
            // Sur un click, il faud*ra* exécuter un put_note avec le contenu actuel de rep
            tempfunction = function () {
                // permet de faire une clôture
                var replocal = rep;
                var akalocal = aka;
                var elemt_local = elemt_span;
                elemt_span.addEventListener("click", function () {
                    // là on peut appeler put_note, les paramètres seront les bons au moment de l'exécution
                    put_note(elemt_local, replocal["terme"], replocal["idbde"], replocal["nom"], replocal["prenom"], tosecondstack,replocal["solde"], replocal["time_negatif"], akalocal);
                });
            };
        } else {
            tempfunction = function () {
                // permet de faire une clôture
                var replocal = rep;
                var elemt_local = elemt_span;
                elemt_span.addEventListener("click", function () {
                    // là on peut appeler put_note, les paramètres seront les bons au moment de l'exécution
                    put_conso(replocal["label"], replocal["id"]);
                });
            }
        }
        tempfunction();
        if (display) { // il faut préparer le listener pour afficher la photo du compte
            // Même trick avec la clôture
            if (!buttonList) {
                tempfunction = function () {
                    var replocal = rep;
                    var akalocal = aka;
                    var elemt_local = elemt_span;
                    elemt_span.addEventListener("mouseover", function() {
                        // On appelle displayAccount dans la clôture
                        displayAccount(elemt_local, replocal["terme"], replocal["idbde"],replocal["solde"], replocal["time_negatif"], akalocal);
                    });
                };
            } else {
                tempfunction = function () {
                    var replocal = rep;
                    var elemt_local = elemt_span;
                    elemt_span.addEventListener("mouseover", function() {
                        // On appelle displayAccount dans la clôture
                        displayButton(replocal["label"], replocal["montant"]/100.0 + " €", replocal["description"], replocal["consigne"]);
                    });
                };
            }
            tempfunction();
        }
        // on ajoute l'élément à la liste
        elemt_span.appendChild(texte);
        elemt.appendChild(elemt_span);
        liste.appendChild(elemt);
    }
    
    old_liste = document.getElementById(targetid)
    // on lui donne le même id qu'avant pour pouvoir recommencer
    liste.setAttribute("id", targetid)
    old_liste.parentNode.replaceChild(liste, old_liste);
}
