Configuration requise
--------------------

#### Paquets requis :

Sous Debian:

* gettext -> Pour la traduction


Comment enrichir la traduction
------------------------------

Pour commencer, il vous faut vous placer à la racine du répertoire,
c'est à dire à l'emplacement de ce README ou dans le sous-dossier note/.

En effet, l'utilitaire Django parcourt uniquement l'arborescence descendante.

Pour détecter toutes les chaînes de textes à traduire, il suffit d'utiliser la commande '''makemessages'''.

django-admin makemessages

ou

python manage.py makemessages

makemessages parcours les fichiers ( par défaut .py, .html, .txt ) à la rehcherche de chaîne de textes à traduire.

L'utilitaire va régenérer le fichier contenant toutes les traductions.
Ce dernier se nomme django.po et se situe dans locale/code_langue/LC_MESSAGES/.

Il vous suffit ensuite d'ajouter la traduction de la chaine en dessous, dans le msgstr.

Dans les fichiers python
------------------------

Ajouter en import du fichier

'from django.utils.translation import ugettext_lazy as _ '

Pour comprendre ce que fait cette fonction, et découvrir ses dérivés :
https://docs.djangoproject.com/fr/1.9/topics/i18n/translation/

Pour déclarer une chaine de texte ou une variable comme à traduire

    Original                  À traduire
"ma chaine de texte" -> _(u"ma chaine de texte")

Lorsque que vous faites ceci, cela informe l'utilitaire Django que vous souhaitez traduire cette chaine de texte.

Notez que vous pouvez aussi mettre des variables, ou des expressions non encore évalués.

Quand vous créer un nouveau template
------------------------------------

Pensez à ajouter la ligne suivante :
{% load i18n %}
Cela chargera les outils de traduction fourni par Django.

Pour des textes simples, vous pourrez utilisez le '''tag trans''' :
{% trans "Mon texte ici" %}
où "Mon texte ici" est le texte à traduire

Pour des textes plus compliqués, on utilisera '''blocktrans''' :
{% blocktrans %}
Le texte,
sur plusieurs lignes.
{% endblocktrans %}

ou comprenant des variables :
{% blocktrans with newvar=ildvar %}
Mon nom est {{ newvar }}.
{% endblocktrans %}

Dans un fichier javascript
--------------------------
https://docs.djangoproject.com/fr/1.9/topics/i18n/translation/#internationalization-in-javascript-code

Do no do it. please.
srsly

Pour déclarer du texte à traduire dans un fichier javascript, on utilise

   Original                  À traduire
"ma chaine de texte" -> gettext("ma chaine de texte")

Pour détecter les chaînes de textes en javascript, il faut les lignes de commandes suivantes où se trouvent les fichiers à traduire -- mais je ne sais pas pourquoi-- :
Dans notre cas il faudra donc allez dans /static/js/custom/.

django-admin makemessages --domain=djangojs

ou

python manage.py makemessages --domain=djangojs

La commande va alors générer un djangojs.po, qui fonctionne comme les django.po.

Remarque:
La ligne suivante a été rajoutée dans la base.html. On a besoin, sur toutes les pages où des scripts sont appelés, d'appeler en parallèle le dictionnaire contenant les traductions javacript. Pour éviter de malheureux oubli...

<script type="text/javascript" src="{% url 'javascript_catalog' %}"></script>

Appliquer vos traductions
-------------------------
Un fois que vous traduit et complétez le django.po, il faut encore compilez le fichier.

Pour cela:

django-admin compilemessages

Cette commande va créer un django.mo qui sera lu lors de la traduction par le serveur.

Comment ajouter une nouvelle langue à traduire
----------------------------------------------

D'abord, dans le settings.py, il vous faut rajouter la langue voulue au dictionnaire LANGUAGES

LANGUAGES = [
    ('fr', _(u"Français")),
    ('en', _(u"Anglais")),
]

Par exemple, si l'on souhaite rajouter le portugais :

LANGUAGES = [
    ('fr', _(u"Français")),
    ('en', _(u"Anglais")),
    ('pt', _(u"Portugais")),
]


Il faut maintenant créer le fichier dans lequel seront stockés
les traductions dans la nouvelle langue.

Pour ceci, lancer la commance suivante :

django-admin makemessages --locale=code_langue

Par exemple, pour du portugais brésilien :

django-admin makemessages --locale=pt_BR

Django devrait créer l'arborescence et le fichier django.po nécéssaire
dans le dossier locale/
Si ce n'est pas le cas, vous pouvez la créer à la main.
