#!/usr/bin/env python
# -*- encoding: utf-8 -*-
import os
import sys

# À partir de Django 1.7, on doit faire le setup si on veut pouvoir
# importer les modules avant d'avoir chargé toutes les apps
# Et on a besoin de connaître le nom du module settings pour cela
import django


os.environ["DJANGO_SETTINGS_MODULE"] = "note.settings"
django.setup()

if __name__ == "__main__":
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "note.settings")

    from django.core.management import execute_from_command_line

    execute_from_command_line(sys.argv)
