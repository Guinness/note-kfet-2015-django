#!/usr/bin/env python
# -*- encoding: utf-8 -*-

"""Les "pages" qui sont destinées à répondre aux requêtes AJAJ."""

import json

# L'objet de réponse HTTP
from django.http import HttpResponse

# Les formulaires
import forms
# Les utilitaires
import utilities
# La communication avec le backend
import nk

# Pour bypasser le test de Cross-Site Request Forgery
from django.views.decorators.csrf import csrf_exempt

# TODO: vérifier que les exempts suivants peuvent être retirés sans risques
@csrf_exempt
def quick_search(request, mode="basic"):
    """Renvoie l'objet JSON résultat d'un quick_search,
       destiné à être chargé par javascript"""
    if (request.method == "GET") or (request.session.get("logged", None) != "ok"):
        return HttpResponse("Get the fuck out of here", status=444)
    else:
        try:
            asked = request.POST["asked"]
        except:
            return HttpResponse("")
        success, sock_ou_response = nk.socket_still_alive(request)
        if success:
            sock = sock_ou_response
            data = [asked, "x"] if mode == "dons" else [asked]
            sock.write(json.dumps(["quick_search", data]))
            out = nk.full_read(sock)["msg"]
            return HttpResponse(json.dumps(out))
        else:
            response = sock_ou_response
            return HttpResponse(u'"Erreur"')

@csrf_exempt
def get_display_info(request):
    """Renvoie l'objet JSON résultat d'un get_display_info
       destiné à être chargé par javascript"""
    if (request.method == "GET") or (request.session.get("logged", None) != "ok"):
        return HttpResponse("Get the fuck out of here", status=444)
    else:
        try:
            asked = json.loads(request.POST["asked"])
        except:
            return HttpResponse("")
        success, sock_ou_response = nk.socket_still_alive(request)
        if success:
            sock = sock_ou_response
            data = asked
            sock.write(json.dumps(["get_display_info", data]))
            out = nk.full_read(sock)["msg"]
            return HttpResponse(json.dumps(out))
        else:
            HttpResponse(u'"Erreur"')

@csrf_exempt
def search(request):
    """Renvoie l'objet JSON résultat d'un search,
       destiné à être chargé par javascript"""
    if (request.method == "GET") or (request.session.get("logged", None) != "ok"):
        return HttpResponse("Get the fuck out of here", status=444)
    else:
        try:
            asked = request.POST["asked"]
        except:
            return HttpResponse("")
        success, sock_ou_response = nk.socket_still_alive(request)
        if success:
            sock = sock_ou_response
            # case-insensitive, inclure les comptes non à jour d'adhésion,
            # afficher les alias, rechercher sur les alias,
            # afficher les anciens pseudos, rechercher sur les anciens pseudos
            flags = "ioaAhH"
            data = [flags, ["idbde", "nom", "prenom", "pseudo", "mail"], asked]
            sock.write(json.dumps(["search", data]))
            out = nk.full_read(sock)["msg"]
            return HttpResponse(json.dumps(out))
        else:
            response = sock_ou_response
            return response


@csrf_exempt
def search_readhesion(request):
    """Renvoie l'objet JSON résultat d'un search,
       destiné à être chargé par javascript"""
    if (request.method == "GET") or (request.session.get("logged", None) != "ok"):
        return HttpResponse("Get the fuck out of here", status=444)
    else:
        try:
            asked = request.POST["asked"]
        except:
            return HttpResponse("")
        success, sock_ou_response = nk.socket_still_alive(request)
        if success:
            sock = sock_ou_response
            flags = "ioaAhH" # dans la recherche de base on ne cherhe pas sur les alias et les historiques, mais on prend les comptes non à jour
            data = [flags, ["idbde", "nom", "prenom", "pseudo", "mail"], asked]
            sock.write(json.dumps(["search", data]))
            out = nk.full_read(sock)["msg"]
            return HttpResponse(json.dumps(out))
        else:
            response = sock_ou_response
            return response


@csrf_exempt
def get_boutons(request, flags=""):
    """Renvoie l'objet JSON résultat d'un get_boutons,
       destiné à être chargé par javascript"""
    if (request.method == "GET") or (request.session.get("logged", None) != "ok"):
        return HttpResponse("Get the fuck out of here", status=444)
    else:
        try:
            asked = request.POST.get("asked", "")
            categorie = request.POST.get("categorie", "")
        except:
            return HttpResponse("HTTP/1.1 Bad Request", status=400)
        success, sock_ou_response = nk.socket_still_alive(request)
        if success:
            sock = sock_ou_response
            data = [asked, categorie, flags]
            sock.write(json.dumps(["get_boutons", data]))
            out = nk.full_read(sock)["msg"]
            return HttpResponse(json.dumps(out))
        else:
            response = sock_ou_response
            return response

@csrf_exempt
def do_conso(request):
    """Effectue les consos envoyées en POST par javascript"""
    if (request.method == "GET") or (request.session.get("logged", None) != "ok"):
        return HttpResponse("Get the fuck out of here", status=444)
    else:
        try:
            consodata = request.POST["consodata"]
            consodata = [[int(i) for i in triplet.split(",")] for triplet in consodata[1:-1].split("),(")]
        except:
            return HttpResponse(u'"Bad request"', status=400)
        success, sock_ou_response = nk.socket_still_alive(request)
        if success:
            sock = sock_ou_response
            sock.write(json.dumps(["consos", consodata]))
            out = nk.full_read(sock)
            return HttpResponse(json.dumps(out))
        else:
            return HttpResponse(u'"Erreur"')

@csrf_exempt
def get_photo(request, idbde=None):
    """Affiche dans une page HTML la photo demandée.
       Si elle n'existe pas encore (ou n'est pas à jour), la demande au serveur
       et la stocke dans un ficher."""
    try:
        idbde = int(idbde)
    except Exception as e:
        return HttpResponse("Bad Request", status=400)
    # On appelle alors la fonction standard
    success, sock_ou_response, variables_standard = utilities.get_varsock(request, socket=True)
    if success:
        sock = sock_ou_response
        utilities._provide_photo(sock, idbde)
        urlphoto = utilities._get_url_photo(idbde)
        return HttpResponse('<img src = "%s"></img>' % (urlphoto))
    else:
        return HttpResponse(u'"Erreur"')

@csrf_exempt
def do_credit_retrait(request, action):
    """Gestion de la requête AJAJ pour un crédit ou un retrait."""
    types = {"especes": "Espèces", "cheque": "Chèque", "virement": "Virement bancaire", "cb" : "Carte bancaire"}
    actions_write = {"credit": "crédit", "retrait": "retrait"}
    actions_socket = {"credit": "crediter", "retrait": "retirer"}
    # On appelle la fonction standard
    success, sock_ou_response, variables_standard = utilities.get_varsock(request, socket=True)
    if success:
        sock = sock_ou_response
        # on n'a pas besoin du prefix parce que le JS a filé les paramètre sans.
        form = forms.CreditRetraitForm(request.POST, label_suffix=" :")
        try:
            ok = form.is_valid()
        except forms.CreditRetraitWithoutIdbde:
            errmsg = u"Il faut penser à cliquer sur un compte avant de créditer/retirer."
            return HttpResponse(errmsg)
        if ok:
            data = form.cleaned_data
            if (data["type"] in ["cheque", "virement"] and "" in [data["nom"], data["prenom"], data["banque"]]):
                return HttpResponse("""Pour un %s par %s, les champs Nom, Prénom et Banque doivent être spécifiés.""" % (actions_write[action], types[data["type"]]))
            if (data["type"] in ["cb"] and "" in [data["nom"], data["prenom"]]):
                return HttpResponse("""Pour un %s par %s, les champs Nom et Prénom doivent être spécifiés.""" % (actions_write[action], types[data["type"]]))
            to_send = [data["idbde"], data["montant"], data["type"],
                       {"nom": data["nom"], "prenom": data["prenom"], "banque": data["banque"], "commentaire" : data["commentaire"]}]
            paquet = [actions_socket[action], to_send]
            sock.write(json.dumps(paquet))
            out = nk.full_read(sock)
            return HttpResponse(json.dumps(out))
        else:
            errmsg = "Ce %s est invalide :\n" % (actions_write[action])
            for (k,v) in form.errors.items():
                errmsg += "%s : %s\n" % (k,"".join([str(i) for i in v]))
            return HttpResponse(errmsg)
    else:
        return HttpResponse(u'"Erreur"')

def do_transfert(request):
    """Gestion de la requête AJAJ pour un transfert."""

    # On appelle la fonction standard
    success, sock_ou_response, variables_standard = utilities.get_varsock(request, socket=True)
    if success:
        sock = sock_ou_response
        if request.method != "POST" or not request.POST.has_key("transfertdata"):
            return HttpResponse("Bad request", status=400)
        transfertdata = request.POST["transfertdata"]
        try:
            transfertdata = json.loads(transfertdata)
        except ValueError:
            return HttpResponse("Failed to decode JSON object.", status=500)
        if not( isinstance(transfertdata, list) and
                [type(i) for i in transfertdata] == [bool, list, list, unicode, unicode]):
            return HttpResponse("Bad parameter %r" % [type(i) for i in transfertdata], status=500)
        is_don, emetteurs, destinataires, montant, commentaire = transfertdata

        # Formattage des champs
        try:
            montant = float(montant)
        except ValueError:
            return HttpResponse(json.dumps({"retcode" : 1111, "msg" : None, "errmsg" : """Transfert impossible : "%s" n'est pas un montant valide.""" % (montant,)}))
        if emetteurs == []:
            return HttpResponse(json.dumps({"retcode" : 1112, "msg" : None, "errmsg" : "Transfert impossible : pas d'émetteurs."}))
        if destinataires == []:
            return HttpResponse(json.dumps({"retcode" : 1113, "msg" : None, "errmsg" : "Transfert impossible : pas de destinataires."}))
        montant = int(round(montant * 100))

        # Sanity check: si don, alors emetteurs devrait être
        if is_don and set(emetteurs) != {request.session["whoami"]["idbde"]}:
            return HttpResponse("emetteurs invalide pour dons", status=403)

        # Forgeons le paquet à envoyer
        if is_don:
            paquet = ["dons", [destinataires, montant, commentaire]]
        else:
            paquet = ["transferts", [emetteurs, destinataires, montant, commentaire]]
        
        # Et on envoie le tout
        sock.write(json.dumps(paquet))
        out = nk.full_read(sock)
        return HttpResponse(json.dumps(out))
    else:
        return HttpResponse(u'"Erreur"')

