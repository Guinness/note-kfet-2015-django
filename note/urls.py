#!/usr/bin/env python
# -*- encoding: utf-8 -*-

""" Associe chaque url à une fonction de :py:mod:`note.views` qui
    est chargée de générer la page correspondante.
"""

from django.conf.urls import patterns, include, url

# Import pour la traduction du javascript
from django.views.i18n import javascript_catalog

#: Liste des patterns d'url
urlpatterns = patterns('note.views',
    # pages de base
    url(ur'^/*$', 'login_page'),
    url(ur'^/index/*$', 'index'),
    url(ur'^/logout/*$', 'logout'),
    # Page pour changer la langue
    url(ur'^i18n/', include('django.conf.urls.i18n')),
    # consos
    url(ur'^/consos(?P<double>-double)?/*$', 'consos'),
    url(ur'^/consos(?:-double)?/(?P<de>de)?valider_transaction/(?P<idtransaction>[^/]*)$', 'toggle_transaction'),
    # dons
    url(ur'^/(?:virements|dons)/*', 'dons'),
    # les activités et invitations
    url(ur'^/(?:activite|invitation)s?(?P<admin>/admin)?/*$', 'activites'),
    url(ur'^/(?:activite|invitation)s?/(?P<idact>[^/]*)/gestion(?P<validation>/validate|/invalidate|/delete)?/*$', 'activite_gestion'),
    url(ur'^/(?:activite|invitation)s?/(?P<idact>[^/]*)/gestion/modifier/*$', 'activite_gestion_modifier'),
    url(ur'^/(?:activite|invitation)s?/(?P<idact>[^/]*)(?P<admin>/admin)?/*$', 'activite'),
    url(ur'^/(?:activite|invitation)s?/(?P<idact>[^/]*)/del_invite/(?P<idinv>[^/]*)(?P<admin>/admin)?/*$', 'del_invite'),
    # mes_activités = création d'activités
    url(ur'^/mes_activites(?:/(?P<idact>[^/]*))?(?P<delete>/delete)?/*$', 'mes_activites'),
    # la recherche et gestion des comptes
    url(ur'^/comptes_advanced/*$', 'comptes_advanced'),
    url(ur'^/search_historique_pseudo/*$', 'search_historique_pseudo'),
    url(ur'^/comptes/(?P<idbde>[^/]*)/*$', 'comptes'),
    url(ur'^/comptes/(?P<idbde>[^/]*)/historique/(?P<num_page>\d+)/*$', 'historique_transactions'),
    url(ur'^/comptes/(?P<idbde>[^/]*)/historique/*$', 'historique_transactions'),
    url(ur'^/comptes/(?P<idbde>[^/]*)/modifier(/listedroits)?/*$', 'modifier_compte'),
    url(ur'^/comptes/(?P<idbde>[^/]*)/supprimer/*$', 'supprimer_compte'),
    url(ur'^/comptes/(?P<idbde>[^/]*)/modifier/password/*$', 'password'),
    url(ur'^/comptes/(?P<idbde>[^/]*)/modifier/photo/*$', 'update_photo'),
    url(ur'^/comptes/(?P<idbde>[^/]*)/aliases/*$', 'aliases'),
    url(ur'^/comptes/(?P<idbde>[^/]*)/aliases/delete(?P<delall>_all)?/(?P<idalias>[^/]*)/*$', 'unalias'),
    url(ur'^/comptes/(?P<idbde>[^/]*)/historique_pseudo/*$', 'historique_pseudo'),
    #les réadhésions
    url(ur'^/readhesions/(?P<idbde>[^/]*)/*$', 'readhesions'),
    # la gestion des boutons
    url(ur'^/boutons/*$', 'boutons'),
    url(ur'^/boutons/(?P<idbouton>[^/]*)(?P<delete>/delete)?/*$', 'boutons'),
    # les préinscription
    url(ur'^/preinscriptions?/*$', 'preinscriptions'),
    # les inscriptions
    url(ur'^/inscriptions?(?:/(?P<preid>[^/]*))?(?P<delete>/delete)?/*$', 'inscriptions'),
    # Application WEI
    url(ur'^/wei/*$', 'WEIaccueil'),
    url(ur'^/wei/monInscription/*$', 'WEImonInscription'),
    url(ur'^/wei/1a/*$', 'WEI1A'),
    url(ur'^/wei/vieux/*$', 'WEIvieux'),
    url(ur'^/wei/admin/*$', 'WEIAdmin'),
    url(ur'^/wei/inscrits/*$', 'WEIinscrits'),
    url(ur'^/wei/inscrits/readherer/(?P<idwei>\d+)/*', 'WEIreadherer'),
    url(ur'^/wei/inscrits/(?P<idwei>\d+)/*$', 'WEIchangeInscription'),
    url(ur'^/wei/inscrits1a/*$', 'WEIinscrits1A'),
    url(ur'^/wei/inscrits1a/(?P<idwei>\d+)/*$', 'WEIchangeInscription1A'),
    url(ur'^/wei/inscrits1a/adherer/(?P<idwei>\d+)/*$', 'WEIcreerCompte1A'),
    # interface trésorerie
    url(ur'^/tresorerie/*$','TresorerieAccueil'),
    url(ur'^/tresorerie/cheques/*$','TresorerieCheques'),
    url(ur'^/tresorerie/remises/*$','TresorerieRemises'),
    url(ur'^/tresorerie/(?P<idtransaction>[^/]*)/(?P<action>ajout|delete)/*$','TresorerieAjoutRemise'),
    url(ur'^/tresorerie/remises/(?P<idremise>[^/]*)?(?P<clore>/clore)?/*$','TresorerieCloreRemise'),
    url(ur'^/tresorerie/facturation/*$','TresorerieFacturation'),
    # regeneration du password
    url(ur'^/regen_pw/(?P<token>[^/]*)/*$', 'regen_pw'),
    # easter egg
    url(ur'^/(?:teapot|the|tea|coffee|cafe)/*$', 'teapot'),
    # Page de liste des droits
    url(ur'^/listedroits/*$','liste_droits')
)

urlpatterns += patterns('note.ajaj',
    # les pages de requêtes AJAJ
    url(ur'^/quick_search_(?P<mode>basic|dons)/*$', 'quick_search'),
    url(ur'^/search/*$', 'search'),
    url(ur'^/search_readhesion/*$', 'search_readhesion'),
    url(ur'^/get_boutons/(?P<flags>[^/]*)/*$', 'get_boutons'),
    url(ur'^/get_display_info/*$', 'get_display_info'),
    url(ur'^/get_photo/(?P<idbde>[^/]*)/*$', 'get_photo'),
    url(ur'^/do_conso/*$', 'do_conso'),
    url(ur'^/do_(?P<action>credit|retrait)/*$', 'do_credit_retrait'),
    url(ur'^/do_transfert/*$', 'do_transfert'),
)



js_info_dict = {
    'packages': ('note'),
}

# Pour la traduction du javascript
urlpatterns += patterns('',
    url(r'^jsi18n/$', javascript_catalog, js_info_dict, name='javascript_catalog'),
)
