#!/usr/bin/env python
# -*- encoding: utf-8 -*-

"""Génération d'erreurs/warnings/success messages.
   
   Ils sont enregistrés dans la session (:py:mod:`django.contrib.messages`)
   pour être affichés à la prochaine page chargée (juste après en général).
   
   """

# Permet de stocker des messages dans la session courante
from django.contrib import messages

#: Import pour la traduction
from django.utils.translation import ugettext_lazy as _


def add_message(request, messagelevel, message):
    """Enregistre un message dans la session avec :py:meth:`django.contrib.messages.add_message`."""
    messages.add_message(request, messagelevel, message)

def add_error(request, message):
    """Enregistre une erreur"""
    add_message(request, messages.ERROR, message)

def add_warning(request, message):
    """Enregistre un warning"""
    add_message(request, messages.WARNING, message)

def add_success(request, message):
    """Enregistre un succès"""
    add_message(request, messages.SUCCESS, message)


## Messages d'erreur
ERRMSG_NK2015_DOWN = _(u"Le Serveur NK2015 est down.")
ERRMSG_NK2015_NOT_RESPONDING = _(u"La connexion avec le serveur NK2015 ne répond pas. Essaye de te reconnecter.")
ERRMSG_HELLO_FAILED = _(u"La version du site utilisée n'est pas supportée par le serveur NK2015.")
ERRMSG_UNKOWNERROR = _(u"Une fucking erreur inconnue s'est produite :")

ERRMSG_IDBDE_INVALID = u'''"%s" n'est pas un identifiant de compte valide.'''
ERRMSG_IDBDE_FAIL = _(u"Le compte n°%s n'existe pas.")
ERRMSG_IDACT_INVALID = u'''"%s" n'est pas un identifiant d'activité valide.'''
ERRMSG_IDACT_FAIL = _(u"L'activité n°%s n'existe pas.")
ERRMSG_IDALIAS_INVALID = u'''"%s" n'est pas un identifiant d'alias valide.'''
ERRMSG_IDINV_INVALID = u'''"%s" n'est pas un identifiant d'invité valide.'''
ERRMSG_IDINV_FAIL = _(u"L'invité n°%s n'existe pas.")
ERRMSG_IDBUTTON_INVALID = u'''"%s" n'est pas un identifiant de bouton valide.'''
ERRMSG_IDBUTTON_FAIL = _(u"Le bouton n°%s n'existe pas.")
ERRMSG_PREID_INVALID = u'''"%s" n'est pas un identifiant de préinscription valide.'''
ERRMSG_PREID_FAIL = _(u"La préinscription n°%s n'existe pas.")
ERRMSG_IDTRANSACTION_INVALID = u'''"%s" n'est pas un identifiant de transaction valide.'''
ERRMSG_IDTRANSACTION_FAIL = _(u"La transaction n°%s n'existe pas.")
ERRMSG_IDREMISE_INVALID = u'''"%s" n'est pas un identifiant de remise valide.'''
ERRMSG_IDREMISE_FAIL = _(u"La remise n°%s n'existe pas.")


ERRMSG_DJANGO_SESSION_EXPIRED = _(u"Ta session Django a expiré, reconnecte-toi.")
ERRMSG_NOSOCKET = _(u"La connexion avec le serveur NK2015 a été perdue, reconnecte-toi.")
ERRMSG_NK2015_SESSION_EXPIRED = _(u"Ta session NK2015 est invalide ou a expiré, reconnecte-toi.")

ERRMSG_PASSWORD_NEGATIVE_IDBDE = _(u"Mais oui bien sûr…")
ERRMSG_NO_ACL_CHGPASS = _(u"Tu n'as pas le droit de changer le mot de passe d'un autre compte que le tien.")

## Messages de succès
SUCCMSG_CHGACT = _(u"Activité modifiée avec succès.")
SUCCMSG_DELACT = _(u"Activité supprimée avec succès.")
SUCCMSG_ADDACT = _(u"Activité ajoutée avec succès.")
SUCCMSG_VALIDACT = _(u"Activité validée avec succès.")
SUCCMSG_DEVALIDACT = _(u"Activité dévalidée avec succès.")
SUCCMSG_DELINV = _(u"Invité supprimé avec succès.")
SUCCMSG_ADDINV = _(u"Invité ajouté avec succès.")

SUCCMSG_ACCOUNT_CHANGED = _(u"Compte modifié avec succès.")
SUCCMSG_ACCOUNT_ADDED = _(u"Compte ajouté avec succès.")
SUCCMSG_ACCOUNT_DELETED = _(u"Compte supprimé avec succès.")
SUCCMSG_ALIAS_ADDED = _(u"Alias ajouté avec succès.")
SUCCMSG_ALIAS_DELETED = _(u"Alias supprimé avec succès.")
SUCCMSG_ALIAS_ALLDELETED = _(u"Tous les alias supprimés avec succès.")
SUCCMSG_PASSWORD_CHANGED = _(u"Mot de passe modifié avec succès.")

SUCCMSG_LOGOUT = _(u"Tu t'es déconnecté.")

SUCCMSG_ADDBUTTON = _(u"Bouton ajouté avec succès.")
SUCCMSG_CHGBUTTON = _(u"Bouton modifié avec succès.")
SUCCMSG_DELBUTTON = _(u"Bouton supprimé avec succès.")

SUCCMSG_PREINSCRIPTION_ADDED = _(u"Préinscription ajoutée avec succès.")
SUCCMSG_PREINSCRIPTION_DELETED = _(u"Préinscription supprimée avec succès.")

SUCCMSG_READHESION = _(u"Réadhésion effectuée avec succès.")

SUCCMSG_PHOTO_UPDATED = _(u"Photo modifiée avec succès.")

SUCCMSG_VALIDATE_TRANSACTION = _(u"Transaction validée avec succès.")
SUCCMSG_DEVALIDATE_TRANSACTION = _(u"Transaction dévalidée avec succès.")

SUCCMSG_RESETPSWD = _(u"Un mail vous a été envoyé.")

SUCCMSG_TRANSACTION_ADDED = _(u"Transaction ajoutée à la remise avec succès.")
SUCCMSG_TRANSACTION_REMOVED = _(u"Transaction retirée de la remise avec succès.")
SUCCMSG_REMISE_ADDED = _(u"Remise ajoutée avec succès.")
SUCCMSG_REMISE_CLOSED = _(u"Remise fermée avec succès.")
SUCCMSG_INVOICE_ADDED = _(u"La facture a été éditée.")
